#### 教师们常用的一些软件

- `ZoomIt` 屏幕缩放
- `Everything` 文件检索
- `LICEcap` 录制GIF
- `Q-dir` 多窗口文件操作
- `SpaceSniffer` 硬盘图形化分析
- `winDecrypt` PDF密码清除器